# [ALW2](https://sites.google.com/view/alw2018)

## Deadlines

[27/7 Deadline ALW 2018](https://sites.google.com/view/alw2018/important-dates)

~~[20/7 Deadline ISI 2018](http://isi18.azurewebsites.net/)~~

## TO DO

@Sahlgren skriver artiklarna

~~@timpal0l nya grafer för ISI-artikeln med bara dummy+ngram+lexikon~~

@timpal0l nya grafer för ALW-artikeln där även lexikon finns med som rak linje

@timpal0l testa en ensemble med både ngram och språkmodell (konkatenera vektorer alt ensemble)

~~@timpal0l kan vi testa en Random Forest eller Gradient Boosting på språkmodellsvektorerna?~~ (blev mycket sämre)

@Sahlgren nya 4k-dimensionella vektorer

@timpal0l nya körningar med 4k-vektorerna

@Sahlgren exempel på features från de olika modellerna

~~@fredriko räkna hur stor andel abusive-termer (unionen av alla lexikon) som finns i Storfront vs Reddit~~

~~@fredriko kan du kolla upp state-of-the-art resultat för de olika datamängderna vi använder?~~

~~13/7 Experiment~~

~~25-29/6 Ramverket (språkmodell och klassificerare klara) @Sahlgren @bjopel @fredriko @timpal0l~~

~~21/6 Datakällorna standardiserade @fredriko~~

~~15/6 Gpu-stöd (ominstallera asgard, fixa keras) @timpal0l @fredriko~~