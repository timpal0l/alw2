
# Results
Numbers in parentheses are the results of the naive lexicon-based classification (i.e.,
a document is abusive if it contains any lexicon item).

Note that Lexicon #2 is the only lexicon that, when used as features, consistently outperforms
the naive method using the same lexicon.

## Hatespeech Tweets
* Lexicon #1 average F1 weighted score for hatespeech_tweets: 0.655 (**0.664**)
* Lexicon #2 average F1 weighted score for hatespeech_tweets: **0.782** (0.729)
* Lexicon #3 average F1 weighted score for hatespeech_tweets: 0.635 (**0.647**)
* Lexicon #4 average F1 weighted score for hatespeech_tweets: 0.62 (**0.626**)

## Wikipedia detox aggression
* Lexicon #1 average F1 weighted score for wikipedia_detox_aggression: 0.784 (**0.795**)
* Lexicon #2 average F1 weighted score for wikipedia_detox_aggression: **0.796** (0.751)
* Lexicon #3 average F1 weighted score for wikipedia_detox_aggression: 0.766 (**0.777**)
* Lexicon #4 average F1 weighted score for wikipedia_detox_aggression: 0.74 (**0.749**)

## Wikipedia detox attack
* Lexicon #1 average F1 weighted score for wikipedia_detox_attack: 0.801 (**0.806**)
* Lexicon #2 average F1 weighted score for wikipedia_detox_attack: **0.807** (0.758)
* Lexicon #3 average F1 weighted score for wikipedia_detox_attack: 0.787 (**0.798**)
* Lexicon #4 average F1 weighted score for wikipedia_detox_attack: 0.762 (**0.771**)

## Wikipedia detox toxicity
* Lexicon #1 average F1 weighted score for wikipedia_detox_toxicity: **0.851** (0.831)
* Lexicon #2 average F1 weighted score for wikipedia_detox_toxicity: **0.858** (0.783)
* Lexicon #3 average F1 weighted score for wikipedia_detox_toxicity: 0.82 (**0.824**)
* Lexicon #4 average F1 weighted score for wikipedia_detox_toxicity: 0.8 (**0.804**)


# Output from the program

```
====== Getting data from collection: hatespeech_tweets ======
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/baseLexicon.txt
Extracting features from 11259 texts
Results for using features from Lexicon #1 on collection hatespeech_tweets
[0.64188237 0.62690242 0.6439613  0.66134245 0.65535653 0.67090227
 0.67219101 0.65781081 0.66753966 0.65357174]
Lexicon #1 average F1 weighted score for hatespeech_tweets: 0.655
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/expandedLexicon.txt
Extracting features from 11259 texts
Results for using features from Lexicon #2 on collection hatespeech_tweets
[0.75886332 0.7774621  0.77819317 0.79628589 0.77844188 0.80152399
 0.78559166 0.77644317 0.78604322 0.78319697]
Lexicon #2 average F1 weighted score for hatespeech_tweets: 0.782
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/hatebase_dict.csv
Extracting features from 11259 texts
Results for using features from Lexicon #3 on collection hatespeech_tweets
[0.60635716 0.60689104 0.63045916 0.6372819  0.63429695 0.65789974
 0.65162287 0.6410191  0.64594067 0.63878081]
Lexicon #3 average F1 weighted score for hatespeech_tweets: 0.635
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/refined_ngram_dict.csv
Extracting features from 11259 texts
Results for using features from Lexicon #4 on collection hatespeech_tweets
[0.59373202 0.59699283 0.61168165 0.62588323 0.61611668 0.64271069
 0.62951745 0.62848384 0.63182627 0.62557436]
Lexicon #4 average F1 weighted score for hatespeech_tweets: 0.62



======================================================================
====== Getting data from collection: wikipedia_detox_aggression ======
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/baseLexicon.txt
Extracting features from 115864 texts
Results for using features from Lexicon #1 on collection wikipedia_detox_aggression
[0.79032119 0.77738935 0.78893091 0.78993554 0.77120071 0.80111902
 0.78261987 0.78565705 0.77165225 0.78077792]
Lexicon #1 average F1 weighted score for wikipedia_detox_aggression: 0.784
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/expandedLexicon.txt
Extracting features from 115864 texts
Results for using features from Lexicon #2 on collection wikipedia_detox_aggression
[0.7867064  0.80478379 0.78846993 0.79479771 0.78186474 0.80959816
 0.79384091 0.79110354 0.80954395 0.7965432 ]
Lexicon #2 average F1 weighted score for wikipedia_detox_aggression: 0.796
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/hatebase_dict.csv
Extracting features from 115864 texts
Results for using features from Lexicon #3 on collection wikipedia_detox_aggression
[0.76507415 0.7670671  0.764728   0.76527971 0.76249924 0.76132903
 0.76713708 0.76817623 0.77459033 0.76133647]
Lexicon #3 average F1 weighted score for wikipedia_detox_aggression: 0.766
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/refined_ngram_dict.csv
Extracting features from 115864 texts
Results for using features from Lexicon #4 on collection wikipedia_detox_aggression
[0.73982589 0.74212635 0.74029075 0.74100715 0.73553542 0.73360772
 0.74263319 0.73955628 0.74366607 0.73745735]
Lexicon #4 average F1 weighted score for wikipedia_detox_aggression: 0.74



======================================================================
====== Getting data from collection: wikipedia_detox_attack ======
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/baseLexicon.txt
Extracting features from 115864 texts
Results for using features from Lexicon #1 on collection wikipedia_detox_attack
[0.80058197 0.7929476  0.81044535 0.80429594 0.78799701 0.80994381
 0.79878117 0.79575078 0.81780625 0.79054664]
Lexicon #1 average F1 weighted score for wikipedia_detox_attack: 0.801
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/expandedLexicon.txt
Extracting features from 115864 texts
Results for using features from Lexicon #2 on collection wikipedia_detox_attack
[0.79593552 0.81054337 0.80028051 0.79278072 0.79648877 0.81753555
 0.81416513 0.81078332 0.82773771 0.80335533]
Lexicon #2 average F1 weighted score for wikipedia_detox_attack: 0.807
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/hatebase_dict.csv
Extracting features from 115864 texts
Results for using features from Lexicon #3 on collection wikipedia_detox_attack
[0.78774629 0.78575637 0.78674719 0.78515175 0.78494332 0.78349776
 0.78751276 0.78748455 0.79464296 0.78160535]
Lexicon #3 average F1 weighted score for wikipedia_detox_attack: 0.787
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/refined_ngram_dict.csv
Extracting features from 115864 texts
Results for using features from Lexicon #4 on collection wikipedia_detox_attack
[0.76124552 0.76274063 0.76361213 0.76096098 0.75889557 0.75774981
 0.76572532 0.76275295 0.76639754 0.75940469]
Lexicon #4 average F1 weighted score for wikipedia_detox_attack: 0.762



======================================================================
====== Getting data from collection: wikipedia_detox_toxicity ======
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/baseLexicon.txt
Extracting features from 159686 texts
Results for using features from Lexicon #1 on collection wikipedia_detox_toxicity
[0.85318119 0.85448672 0.85555774 0.84675804 0.85452908 0.84996045
 0.84974454 0.84888274 0.84961452 0.84977857]
Lexicon #1 average F1 weighted score for wikipedia_detox_toxicity: 0.851
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/expandedLexicon.txt
Extracting features from 159686 texts
Results for using features from Lexicon #2 on collection wikipedia_detox_toxicity
[0.86101181 0.85751903 0.86064867 0.85075332 0.85975951 0.85458024
 0.85446972 0.86050803 0.85954974 0.85714245]
Lexicon #2 average F1 weighted score for wikipedia_detox_toxicity: 0.858
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/hatebase_dict.csv
Extracting features from 159686 texts
Results for using features from Lexicon #3 on collection wikipedia_detox_toxicity
[0.81849651 0.82396055 0.822105   0.81568494 0.82280178 0.81726173
 0.82095375 0.81884947 0.81784437 0.81724434]
Lexicon #3 average F1 weighted score for wikipedia_detox_toxicity: 0.82
======================================================================
Using lexicon file: /home/fredols/source/alw2/lexicons/lexicons_preprocessed/refined_ngram_dict.csv
Extracting features from 159686 texts
Results for using features from Lexicon #4 on collection wikipedia_detox_toxicity
[0.7979733  0.80415881 0.80339806 0.79558284 0.80281682 0.80013772
 0.79831436 0.80165777 0.80016607 0.79975618]
Lexicon #4 average F1 weighted score for wikipedia_detox_toxicity: 0.8

```
