# Resultat av att räkna abusive-terms

## Stormfront
```
Results for db stormfront, collection comments
Number of tokens: 660916880
Number of documents: 4596045
Number of lexicon terms: 28667066
Abusive ratio - terms to tokens: 0.04337469183719441
Abusive ratio - terms to documents: 6.237333620536788
```

## Reddit
```
Results for db reddit, collection comments
Number of tokens: 1984806850
Number of documents: 54504409
Number of lexicon terms: 81618512
Abusive ratio - terms to tokens: 0.041121639619492445
Abusive ratio - terms to documents: 1.4974662325024017
```