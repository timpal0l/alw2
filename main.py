from sklearn.dummy import DummyClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier
from sklearn.svm import LinearSVC
from models.baselines import Evaluator
from utils.data_reader import get_training_data_sklearn, get_training_vectors_sklearn


def baseline(collection):
    X, y = get_training_data_sklearn(database='extreme_adopters', collection='comments')
    evaluator = Evaluator(X=X, y=y, folds=10, test_size=.25)
    evaluator.run(model=DummyClassifier, collection_name=collection, make_vectors=True)
    evaluator.run(model=LogisticRegression, collection_name=collection, make_vectors=True)


def transfer_learning(collection):
    X, y = get_training_vectors_sklearn(database='genders_sv', collection='comments', vec_attribute_name='elmo_vector')
    evaluator = Evaluator(X=X, y=y, folds=10, test_size=.25)
    evaluator.run(model=LogisticRegression, collection_name=collection, make_vectors=False)


def run_all_experiments():
    collections = ['hatespeech_tweets', 'wikipedia_detox_aggression',
                   'wikipedia_detox_toxicity', 'wikipedia_detox_attack']

    collections = ['comments', ]

    for collection in collections:
        baseline(collection)
        # transfer_learning(collection)


run_all_experiments()
