from typing import List, Tuple, Union

import shap
from sklearn import metrics
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
from sklearn.linear_model import LogisticRegression
import pandas as pd
from sklearn.externals import joblib
from sklearn.pipeline import FeatureUnion

from utils.data_reader import get_training_data_sklearn
from utils.lexicon_scikit_classifier import FeatureExtractor


def persists_models(database: str, collection: str, lexicon_file: Tuple[str, str], model_dir: str) -> None:
    print("Getting data from database: {}, collection: {}".format(database, collection))
    raw_texts, labels = get_training_data_sklearn(database=database, collection=collection, max_num_documents=None)

    create_and_persist_model(create_full_ngram_vectorizer(), raw_texts, labels, model_dir, "ngram")

    feature_extractor = FeatureExtractor(lexicon_file[0])
    feature_texts = feature_extractor.extract_from_documents(raw_texts)
    create_and_persist_model(create_lexicon_vectorizer(), feature_texts, labels, model_dir, "lexicon")

    create_and_persist_model(create_word_ngram_vectorizer(), raw_texts, labels, model_dir, "word_ngram")


def persist_training_test_data_plain_text(database: str, collection: str, model_dir: str) -> None:
    raw_texts, labels = get_training_data_sklearn(database=database, collection=collection, max_num_documents=None)
    training_documents, testing_documents, training_labels, testing_labels = train_test_split(raw_texts,
                                                                                              labels,
                                                                                              test_size=0.05,
                                                                                              random_state=0,
                                                                                              stratify=labels)
    joblib.dump(training_documents, model_dir + "training_documents_plain_text.pkl")
    joblib.dump(testing_documents, model_dir + "testing_documents_plain_text.pkl")
    joblib.dump(training_labels, model_dir + "training_labels_plain_text.pkl")
    joblib.dump(testing_labels, model_dir + "testing_labels_plain_text.pkl")


def load_training_test_data_plain_text(model_dir: str) -> Tuple[object, object, object, object]:
    training_documents = joblib.load(model_dir + "training_documents_plain_text.pkl")
    testing_documents = joblib.load(model_dir + "testing_documents_plain_text.pkl")
    training_labels = joblib.load(model_dir + "training_labels_plain_text.pkl")
    testing_labels = joblib.load(model_dir + "testing_labels_plain_text.pkl")
    return training_documents, testing_documents, training_labels, testing_labels


def create_full_ngram_vectorizer() -> FeatureUnion:
    char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 4), use_idf=True, norm='l2',
                                      analyzer='char', smooth_idf=True, strip_accents='unicode',
                                      sublinear_tf=True)

    word_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                      analyzer='word', smooth_idf=True, strip_accents='unicode',
                                      sublinear_tf=True)

    features = FeatureUnion([('chars', char_vectorizer), ('words', word_vectorizer)])
    return features


def create_word_ngram_vectorizer() -> TfidfVectorizer:
    return TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                           analyzer='word', smooth_idf=True, strip_accents='unicode',
                           sublinear_tf=True)


def create_lexicon_vectorizer() -> CountVectorizer:
    return CountVectorizer()


def create_and_persist_model(vectorizer: Union[CountVectorizer, FeatureUnion], raw_texts: List[str], labels: List[str],
                             model_dir: str, model_base_name: str) -> None:
    print("==== Training and persisting {} model...".format(model_base_name))
    texts_vectorized = vectorizer.fit_transform(raw_texts)
    as_dataframe = pd.DataFrame(texts_vectorized.todense(), columns=vectorizer.get_feature_names())

    training_documents, testing_documents, training_labels, testing_labels = train_test_split(as_dataframe,
                                                                                              labels,
                                                                                              test_size=0.05,
                                                                                              random_state=0,
                                                                                              stratify=labels)
    target_names = list(set(labels))
    print("Training classifier")
    estimator = LogisticRegression()
    estimator.fit(training_documents, training_labels)
    print("Saving files to disk with base name: {}, to directory: {}".format(model_base_name, model_dir))
    joblib.dump(estimator, model_dir + model_base_name + "_estimator.pkl")
    joblib.dump(vectorizer, model_dir + model_base_name + "_vectorizer.pkl")
    joblib.dump(training_documents, model_dir + model_base_name + "_training_documents.pkl")
    joblib.dump(testing_documents, model_dir + model_base_name + "_testing_documents.pkl")
    joblib.dump(training_labels, model_dir + model_base_name + "_training_labels.pkl")
    joblib.dump(testing_labels, model_dir + model_base_name + "_testing_labels.pkl")
    joblib.dump(target_names, model_dir + model_base_name + "_target_names.pkl")


def unmarshal_and_test_model(model_dir: str, model_base_name: str) -> None:
    print("==== Testing persisted {} model...".format(model_base_name))
    print("Loading model with base name: {},  from directory: {}".format(model_base_name, model_dir))
    testing_documents = joblib.load(model_dir + model_base_name + "_testing_documents.pkl")
    testing_labels = joblib.load(model_dir + model_base_name + "_testing_labels.pkl")
    target_names = joblib.load(model_dir + model_base_name + "_target_names.pkl")
    classifier = joblib.load(model_dir + model_base_name + "_estimator.pkl")
    vectorizer = joblib.load(model_dir + model_base_name + "_vectorizer.pkl")

    ll = sorted(list(zip(vectorizer.get_feature_names(), classifier.coef_[0])), key=lambda x: x[1], reverse=True)
    for l in ll[:20]:
        print(l)
    predicted_labels = classifier.predict(testing_documents)
    print(metrics.classification_report(testing_labels, predicted_labels,
                                        target_names=list(target_names)))

    average_precision, average_recall, f1_weighted, _ = \
        metrics.precision_recall_fscore_support(testing_labels, predicted_labels, average='weighted')
    print("Average precision: {}, average recall: {}, F1 weighted: {}".format(average_precision, average_recall,
                                                                              f1_weighted))


def shap_summary_plot(model_dir: str, model_base_name: str) -> None:
    print("Loading model with base name: {}, from directory: {}".format(model_base_name, model_dir))
    training_documents = joblib.load(model_dir + model_base_name + "_training_documents.pkl")
    testing_documents = joblib.load(model_dir + model_base_name + "_testing_documents.pkl")
    target_names = joblib.load(model_dir + model_base_name + "_target_names.pkl")
    classifier = joblib.load(model_dir + model_base_name + "_estimator.pkl")
    vectorizer = joblib.load(model_dir + model_base_name + "_vectorizer.pkl")
    print("Done loading files")

    print("Initializing shap")
    shap.initjs()
    print("Computing k-means")
    training_documents_summary = shap.kmeans(training_documents, 50)
    print("Setting up explainer")
    explainer = shap.KernelExplainer(classifier.predict_proba, training_documents_summary)
    print("Creating shap values")
    shap_values = explainer.shap_values(testing_documents, nsamples=100)
    print("Plotting findings")
    shap.summary_plot(shap_values, feature_names=vectorizer.get_feature_names(), class_names=target_names)
    # shap.force_plot(explainer.expected_value[0], shap_values[0][0,:], testing_documents.iloc[0,:], link="logit")
    # shap.force_plot(explainer.expected_value[0], shap_values[0], testing_documents, link="logit")


def shap_summary_plot_canned(model_dir: str, model_base_name: str) -> None:
    explainer, shap_values, training_documents_summary = shap_load_data(model_dir, model_base_name)
    vectorizer = joblib.load(model_dir + model_base_name + "_vectorizer.pkl")
    shap.initjs()
    shap.summary_plot(shap_values[0], feature_names=vectorizer.get_feature_names())
    shap.summary_plot(shap_values[1], feature_names=vectorizer.get_feature_names())


def shap_dump_data(model_dir: str, model_base_name: str, use_k_means=True):
    print("Loading model with base name: {}, from directory: {}".format(model_base_name, model_dir))
    training_documents = joblib.load(model_dir + model_base_name + "_training_documents.pkl")
    testing_documents = joblib.load(model_dir + model_base_name + "_testing_documents.pkl")
    classifier = joblib.load(model_dir + model_base_name + "_estimator.pkl")
    print("Done loading files")

    if use_k_means:
        print("Computing k-means")
        training_documents_summary = shap.kmeans(training_documents, 200)
        print("Saving k-means document summary")
        joblib.dump(training_documents_summary, model_dir + model_base_name + "_training_doc_summary.pkl")
        print("Setting up explainer based on training data summary (k-means)")
        explainer = shap.KernelExplainer(classifier.predict_proba, training_documents_summary)
    else:
        print("Setting up explainer based on all training data")
        explainer = shap.KernelExplainer(classifier.predict_proba, training_documents)

    joblib.dump(explainer, model_dir + model_base_name + "_explainer.pkl")
    print("Creating shap values")
    shap_values = explainer.shap_values(testing_documents, nsamples=100)
    joblib.dump(shap_values, model_dir + model_base_name + "_shap_values.pkl")


def shap_load_data(model_dir: str, model_base_name: str) -> Tuple[object, object, object]:
    explainer = joblib.load(model_dir + model_base_name + "_explainer.pkl")
    shap_values = joblib.load(model_dir + model_base_name + "_shap_values.pkl")
    training_documents_summary = joblib.load(model_dir + model_base_name + "_training_doc_summary.pkl")
    return explainer, shap_values, training_documents_summary


def shap_force_plot(model_dir: str, model_base_name: str):
    print("Loading model with base name: {}, from directory: {}".format(model_base_name, model_dir))
    training_documents = joblib.load(model_dir + model_base_name + "_training_documents.pkl")
    testing_documents = joblib.load(model_dir + model_base_name + "_testing_documents.pkl")
    target_names = joblib.load(model_dir + model_base_name + "_target_names.pkl")
    classifier = joblib.load(model_dir + model_base_name + "_estimator.pkl")
    vectorizer = joblib.load(model_dir + model_base_name + "_vectorizer.pkl")
    print("Done loading files")

    print("Initializing shap")
    shap.initjs()
    print("Computing k-means")
    training_documents_summary = shap.kmeans(training_documents, 50)
    print("Setting up explainer")
    explainer = shap.KernelExplainer(classifier.predict_proba, training_documents_summary)
    print("Creating shap values")
    shap_values = explainer.shap_values(testing_documents, nsamples=100)
    print("Plotting findings")
    shap.force_plot(explainer.expected_value, shap_values[0, :], training_documents_summary.iloc[0, :])


class ExampleExplainer(object):

    def __init__(self, model_dir: str, model_base_name: str):
        self.model_dir = model_dir
        self.model_base_name = model_base_name

    def initialize(self) -> None:
        print("Loading data from disk")
        explainer, shap_values, training_documents_summary = shap_load_data(self.model_dir, self.model_base_name)
        testing_documents = joblib.load(self.model_dir + self.model_base_name + "_testing_documents.pkl")
        _, testing_documents_text, _, testing_labels_text = load_training_test_data_plain_text(self.model_dir)
        self.explainer = explainer
        self.shap_values = shap_values
        self.training_documents_summary = training_documents_summary
        self.testing_documents = testing_documents
        self.testing_documents_text = testing_documents_text
        self.testing_labels_text = testing_labels_text
        print("Done loading data")

    def show(self, example_index: int) -> int:
        print("Showing testing example with index: {}".format(example_index))
        print("Text: {}".format(self.testing_documents_text[example_index]))
        print("Label: {}".format(self.testing_labels_text[example_index]))
        return example_index


if __name__ == "__main__":
    database = "alw2018"
    collection = "hatespeech_tweets"
    lexicon_dir = "/home/fredols/source/alw2/lexicons/lexicons_preprocessed/"
    lexicon_file = (lexicon_dir + "expandedLexicon.txt", "Lexicon #2")
    model_directory = "models/k_means_50_clusters/"

    # persists_models(database, collection, lexicon_file, model_directory)
    # unmarshal_and_test_model(model_directory, "ngram")
    # unmarshal_and_test_model(model_directory, "word_ngram")
    # unmarshal_and_test_model(model_directory, "lexicon")
    # shap_dump_data(model_directory, "ngram")
    #shap_dump_data(model_directory, "ngram", use_k_means=True)
    training_doc, testing_doc, training_lab, testing_lab = load_training_test_data_plain_text(model_directory)
