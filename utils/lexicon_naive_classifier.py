import pymongo
from pymongo.collection import Collection
from typing import List, Set
import re
from segtok.segmenter import split_multi
from segtok.tokenizer import web_tokenizer
from nltk.tokenize import MWETokenizer
from sklearn.metrics import classification_report, f1_score


class LexiconClassifier(object):

    def __init__(self, db_url: str, db_name: str, db_collection_name: str, lexicon_file: str, lexicon_url: str):
        self.collection = self.set_up_db(db_url, db_name, db_collection_name)
        self.db_collection_name = db_collection_name
        self.discard_pattern = re.compile("^[,.:;\-_\"\'“”—()\[\]|/!?–`*=]+$")
        self.lexicon: Set = LexiconClassifier.read_lexicon(lexicon_file)
        self.lexicon_file_name: str = lexicon_file
        self.lexicon_url = lexicon_url
        self.mwe_tokenizer = MWETokenizer(separator=" ")
        for entry in self.lexicon:
            if " " in entry:
                self.mwe_tokenizer.add_mwe(tuple(entry.split()))

    @staticmethod
    def read_lexicon(lexicon_file: str) -> Set[str]:
        lexicon = set({})
        with open(lexicon_file, "r") as input:
            lines = input.readlines()
        for line in lines:
            line = line.strip().lower()
            if len(line) > 0:
                lexicon.add(line)
        return lexicon

    def split_and_tokenize(self, document: str) -> List[List[str]]:
        result = []
        sentences = split_multi(document)
        for sentence in sentences:
            s = [token.lower() for token in self.mwe_tokenizer.tokenize(web_tokenizer(sentence)) if
                 not self.discard_pattern.match(token)]
            if len(s) > 0:
                result.append(s)
        return result

    @staticmethod
    def set_up_db(db_url: str, db_name: str, collection_name: str) -> Collection:
        client = pymongo.MongoClient(db_url)
        return client[db_name][collection_name]

    def classify(self, target_names: List[str], result_file: str, verbose=False, is_twitter=False):
        true_labels = []
        predicted_labels = []
        max_process = 200000
        num_processed = 0
        for cursor in self.collection.find():
            num_processed += 1
            if num_processed % 500 == 0:
                print("Processed {}".format(num_processed))
            text = cursor["text"]
            if is_twitter:
                true_label = cursor["external_source"]["label"]
                if true_label == "racism":
                    if verbose:
                        print("Skipping tweet with label \"racism\"")
                    continue
            else:
                true_label = cursor["label"]
            if true_label == "none":
                true_label = 0
            else:
                true_label = 1
            predicted_label = 0
            sentences = self.split_and_tokenize(text)
            for sentence in sentences:
                for token in sentence:
                    if token in self.lexicon:
                        predicted_label = 1
                        if verbose and " " in token:
                            print("token: \"{}\", sentence: {}".format(token, sentence))
                        break
                if predicted_label == 1:
                    break
            true_labels.append(true_label)
            predicted_labels.append(predicted_label)
            if num_processed == max_process:
                break

        with open(result_file, "a") as output:
            output.write("\n=========== {} ===========\n".format(self.db_collection_name))
            output.write("Processed {} documents from collection \"{}\" using lexicon file {}\n"
                         .format(num_processed, self.db_collection_name, self.lexicon_file_name))
            output.write("\nThe lexicon contains {} entries, and the original is available here: {}\n\n".format(
                len(self.lexicon), self.lexicon_url))
            output.write(classification_report(true_labels, predicted_labels, target_names=target_names))
            output.write("\nWeighted F1 score: {}\n".format(f1_score(true_labels, predicted_labels, average="weighted")))
            output.write("\n")

        print("\nProcessed {} document from collection \"{}\" using lexicon file {}"
              .format(num_processed, self.db_collection_name, self.lexicon_file_name))
        print("The lexicon contains {} entries\n".format(len(self.lexicon)))
        print(classification_report(true_labels, predicted_labels, target_names=target_names))
        print("Weighted F1 score: {}".format(f1_score(true_labels, predicted_labels, average="weighted")))


def classify_wikipedia_detox():
    result_file = "/Users/fredriko/Dropbox/data/alw2/lexicon-classification-results-wikipedia-detox-1.txt"

    db_url = "mongodb://localhost:27017"
    db_name = "alw2018"

    # Lexica
    uds_lsv_base = "/Users/fredriko/Dropbox/data/alw2/lexicons/uds-lsv_base_lexicon-only-abusive.txt"
    uds_lsv_base_url = "https://github.com/uds-lsv/lexicon-of-abusive-words/blob/master/Lexicons/baseLexicon.txt"

    uds_lsv_expanded = "/Users/fredriko/Dropbox/data/alw2/lexicons/uds-lsv_expanded_lexicon-only-abusive.txt"
    uds_lsv_expanded_url = "https://github.com/uds-lsv/lexicon-of-abusive-words/blob/master/Lexicons/expandedLexicon.txt"

    t_davidson_base = "/Users/fredriko/Dropbox/data/alw2/lexicons/t-davidson-hatebase_dict.txt"
    t_davidson_base_url = "https://github.com/t-davidson/hate-speech-and-offensive-language/blob/master/lexicons/hatebase_dict.csv"

    t_davidson_expanded = "/Users/fredriko/Dropbox/data/alw2/lexicons/t-davidson-refined_ngram_dict.txt"
    t_davidson_expanded_url = "https://github.com/t-davidson/hate-speech-and-offensive-language/blob/master/lexicons/refined_ngram_dict.csv"

    # Wikipedia detox data labels
    labels = ["attack", "aggression", "toxicity"]

    lexica = [(uds_lsv_base, uds_lsv_base_url), (uds_lsv_expanded, uds_lsv_expanded_url),
              (t_davidson_base, t_davidson_base_url), (t_davidson_expanded, t_davidson_expanded_url)]

    for label in labels:
        target_names = ["none", label]
        db_collection_name = "wikipedia_detox_{}".format(label)
        print("Processing collection: {}".format(db_collection_name))
        for lexicon in lexica:
            print("Using lexicon: {}".format(lexicon))
            classifier = LexiconClassifier(db_url, db_name, db_collection_name, lexicon[0], lexicon[1])
            classifier.classify(target_names, result_file, verbose=False)


def classify_twitter_hatespeech():
    result_file = "/Users/fredriko/Dropbox/data/alw2/lexicon-classification-results-twitter-hatespeech-1.txt"

    db_url = "mongodb://localhost:27017"
    db_name = "alw2018"

    # Lexica
    uds_lsv_base = "/Users/fredriko/Dropbox/data/alw2/lexicons/uds-lsv_base_lexicon-only-abusive.txt"
    uds_lsv_base_url = "https://github.com/uds-lsv/lexicon-of-abusive-words/blob/master/Lexicons/baseLexicon.txt"

    uds_lsv_expanded = "/Users/fredriko/Dropbox/data/alw2/lexicons/uds-lsv_expanded_lexicon-only-abusive.txt"
    uds_lsv_expanded_url = "https://github.com/uds-lsv/lexicon-of-abusive-words/blob/master/Lexicons/expandedLexicon.txt"

    t_davidson_base = "/Users/fredriko/Dropbox/data/alw2/lexicons/t-davidson-hatebase_dict.txt"
    t_davidson_base_url = "https://github.com/t-davidson/hate-speech-and-offensive-language/blob/master/lexicons/hatebase_dict.csv"

    t_davidson_expanded = "/Users/fredriko/Dropbox/data/alw2/lexicons/t-davidson-refined_ngram_dict.txt"
    t_davidson_expanded_url = "https://github.com/t-davidson/hate-speech-and-offensive-language/blob/master/lexicons/refined_ngram_dict.csv"

    lexica = [(uds_lsv_base, uds_lsv_base_url), (uds_lsv_expanded, uds_lsv_expanded_url),
              (t_davidson_base, t_davidson_base_url), (t_davidson_expanded, t_davidson_expanded_url)]

    target_names = ["none", "sexism"]
    db_collection_name = "hatespeech_tweets"
    print("Processing collection: {}".format(db_collection_name))
    for lexicon in lexica:
        print("Using lexicon: {}".format(lexicon))
        classifier = LexiconClassifier(db_url, db_name, db_collection_name, lexicon[0], lexicon[1])
        classifier.classify(target_names, result_file, verbose=True, is_twitter=True)


if __name__ == "__main__":

    classify_wikipedia_detox()