from pymongo import MongoClient

mongo_object = MongoClient('dsg.foi.se')
db = mongo_object['alw2018']

collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']

for collection in collections:

    negatives = []
    positives = []
    cursor = db[collection].find().limit(10000)

    for doc in cursor:
        label = doc['label']
        if label == 'none':
            negatives.append(label)
        else:
            positives.append(label)

    print('collection {} positives {} negatives {}'.format(collection, len(positives), len(negatives)))

