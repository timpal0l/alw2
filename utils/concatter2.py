from pymongo import MongoClient
from scipy import hstack
from sklearn.model_selection import ShuffleSplit
from utils.encoder import Model

from utils.data_reader import get_training_vectors_sklearn

collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']
mongo_object = MongoClient('dsg.foi.se')  # glöm inte å byta db
db = mongo_object['alw2018']

model = Model()

"""for collection in collections:
    print('collection: ', collection)

    for doc in db[collection].find(no_cursor_timeout=True).limit(12000):
        ngrams = doc['reddit_ngrams_vector']
        doc2vec = doc['reddit_doc2vec_vector']

        concat = ngrams + doc2vec

        db[collection].update_one(
            {'_id': doc['_id']},
            {'$set': {'{}_vector'.format('Ngrams-D2V-R'): concat}}, False, True)"""

for collection in collections:
    print('collection: ', collection)

    for doc in db[collection].find(no_cursor_timeout=True).limit(12000):
        raw = doc['text']
        raw_vector = model.transform([raw]).tolist()[0]

        db[collection].update_one(
            {'_id': doc['_id']},
            {'$set': {'{}_vector'.format('SentiNeuron'): raw_vector}}, False, True)
