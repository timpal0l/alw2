from numpy import mean
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import cross_val_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import FeatureUnion, make_pipeline
from data_reader import get_training_data_sklearn

seed = 0
cv = ShuffleSplit(n_splits=5, test_size=0.2, random_state=0)
ensemble = LogisticRegression()
X, y = get_training_data_sklearn(database='impact', collection='comments')

char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 4), use_idf=True, norm='l2',
                                  analyzer='char', smooth_idf=True, strip_accents='unicode',
                                  sublinear_tf=True)

word_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                  analyzer='word', smooth_idf=True, strip_accents='unicode',
                                  sublinear_tf=True)

features = FeatureUnion([('chars', char_vectorizer), ('words', word_vectorizer)])
pipeline_ngrams = make_pipeline(features, ensemble)
pipeline_ngrams.fit(X,y)
print(mean(cross_val_score(X=X, y=y, estimator=ensemble, cv=cv, scoring='f1_weighted')))
