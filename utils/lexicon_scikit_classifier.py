import re
from typing import Set, List

from nltk import MWETokenizer
from segtok.segmenter import split_multi
from segtok.tokenizer import web_tokenizer
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import cross_val_score
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
import numpy as np
from sklearn.pipeline import make_pipeline

from utils.data_reader import get_training_data_sklearn


class FeatureExtractor(object):

    def __init__(self, lexicon_file: str):
        self.discard_pattern = re.compile("^[,.:;\-_\"\'“”—()\[\]|/!?–`*=]+$")
        self.lexicon: Set = FeatureExtractor.read_lexicon(lexicon_file)
        self.lexicon_file = lexicon_file
        self.mwe_tokenizer = MWETokenizer(separator=" ")
        for entry in self.lexicon:
            if " " in entry:
                self.mwe_tokenizer.add_mwe(tuple(entry.split()))

    @staticmethod
    def read_lexicon(lexicon_file: str) -> Set[str]:
        lexicon = set({})
        with open(lexicon_file, "r") as input:
            lines = input.readlines()
        for line in lines:
            line = line.strip().lower()
            if len(line) > 0:
                lexicon.add(line)
        return lexicon

    def split_and_tokenize(self, document: str) -> List[List[str]]:
        result = []
        sentences = split_multi(document)
        for sentence in sentences:
            s = [token.lower() for token in self.mwe_tokenizer.tokenize(web_tokenizer(sentence)) if
                 not self.discard_pattern.match(token)]
            if len(s) > 0:
                result.append(s)
        return result

    def extract_from_document(self, document: str) -> str:
        sentences = self.split_and_tokenize(document)
        reduction = []
        for sentence in sentences:
            for token in sentence:
                if token in self.lexicon:
                    reduction.append(token.replace(" ", "_"))
        if len(reduction) > 0:
            return " ".join(reduction)
        else:
            return ""

    def extract_from_documents(self, documents: List[str]) -> List[str]:
        print("Extracting features from text based on lexicon file: {}".format(self.lexicon_file))
        features = []
        for document in documents:
            features.append(self.extract_from_document(document))
        return features


def predict_cv():
    DATABASE = 'alw2018'
    collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']

    lexicon_dir = "/home/fredols/source/alw2/lexicons/lexicons_preprocessed/"

    lexicon_files = [(lexicon_dir + "baseLexicon.txt", "Lexicon #1"),
                     (lexicon_dir + "expandedLexicon.txt", "Lexicon #2"),
                     (lexicon_dir + "hatebase_dict.csv", "Lexicon #3"),
                     (lexicon_dir + "refined_ngram_dict.csv", "Lexicon #4")]

    for collection in collections:

        print("======================================================================")
        print("====== Getting data from collection: {} ======".format(collection))
        raw_texts, labels = get_training_data_sklearn(database=DATABASE, collection=collection, max_num_documents=None)

        for lexicon_file in lexicon_files:
            print("======================================================================")
            print("Using lexicon file: {}".format(lexicon_file[0]))
            feature_extractor = FeatureExtractor(lexicon_file[0])
            print("Extracting features from {} texts".format(len(raw_texts)))
            feature_texts = feature_extractor.extract_from_documents(raw_texts)

            vectorizer = CountVectorizer()
            estimator = LogisticRegression()
            clf_pipeline = make_pipeline(vectorizer, estimator)

            shuffle_split_cv = ShuffleSplit(n_splits=10, test_size=0.2, random_state=0)

            shuffle_split_scores = cross_val_score(clf_pipeline, feature_texts, labels, cv=shuffle_split_cv, scoring="f1_weighted")
            print("Results for using features from {} on collection {}".format(lexicon_file[1], collection))
            print(shuffle_split_scores)
            print("{} average F1 weighted score for {}: {}".format(lexicon_file[1], collection, round(np.mean(shuffle_split_scores), 3)))

            """
            stratified_shuffle_split_cv = StratifiedShuffleSplit(n_splits=10, test_size=0.2, random_state=0)
            stratified_shuffle_split_scores = cross_val_score(clf_pipeline, feature_texts, labels, cv=stratified_shuffle_split_cv, scoring="f1_weighted")
            print("STRATIFIED SHUFFLE SPLIT: Results for using features from {} on collection {}".format(lexicon_file[1], collection))
            print(stratified_shuffle_split_scores)
            print("Mean: {}".format(np.mean(stratified_shuffle_split_scores)))
            """

        print("\n\n")


if __name__ == "__main__":
    predict_cv()
