import warnings

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.pipeline import make_pipeline, FeatureUnion
from pymongo import MongoClient
from utils.data_reader import get_training_data_sklearn

mongo_object = MongoClient('dsg.foi.se')
db = mongo_object['alw2018']
collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']
collection = 'hatespeech_tweets'

warnings.filterwarnings('ignore')
print('Loading data')
X, y = get_training_data_sklearn(database='reddit', collection='comments', text_attribute_name='text',
                                 label_attribute_name=None, shuffle_data=False, max_num_documents=5000000)

char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 4), use_idf=True, norm='l2',
                                  analyzer='char', smooth_idf=True, strip_accents='unicode',
                                  sublinear_tf=True, max_features=5000)

word_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                  analyzer='word', smooth_idf=True, strip_accents='unicode',
                                  sublinear_tf=True, max_features=5000)

features = FeatureUnion([('chars', char_vectorizer), ('words', word_vectorizer)])

pipeline = make_pipeline(features)

print('Fitting transformer')
pipeline.fit(X)

for e, doc in enumerate(db[collection].find(no_cursor_timeout=True)):
    print(e)
    db[collection].update_one({'_id': doc['_id']}, {
        '$set': {'{}_vector'.format('reddit_ngrams'): pipeline.transform([doc['text']]).toarray()[0].tolist()}}, False,
                              True)
