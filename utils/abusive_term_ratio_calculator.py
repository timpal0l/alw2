from os import listdir
from os.path import isfile, join
from typing import Set, List
import pymongo
from pymongo.collection import Collection
from nltk.tokenize import word_tokenize, MWETokenizer

"""
Task: 
@fredriko räkna hur stor andel abusive-termer (unionen av alla lexikon) som finns i Storfront vs Reddit 
"""


def set_up_db(db_url: str, db_name: str, collection_name: str) -> Collection:
    client = pymongo.MongoClient(db_url)
    return client[db_name][collection_name]


def get_lexicon_terms(lexicon_dir: str) -> Set[str]:
    """
    :param lexicon_dir: the path to the directory in which the lexicon files reside.
    :return: A set representing the union of all terms in the lexicon files.
    """
    lexica: List[str] = [join(lexicon_dir, f) for f in listdir(lexicon_dir) if isfile(join(lexicon_dir, f))]
    terms: Set[str] = set()
    for lexicon in lexica:
        with open(lexicon, "r") as infile:
            terms.update({term.strip().lower() for term in infile.readlines()})
    return terms


def process(db_url: str, db_name: str, db_collection: str, lexicon_dir: str, text_field: str ="text"):
    # Set up the database, to fail-fast if necessary
    collection = set_up_db(db_url, db_name, db_collection)

    # Get the union of the contents of the lexica
    terms = get_lexicon_terms(lexicon_dir)

    # Set up the multi-word tokenizer
    mwe_tokenizer = MWETokenizer(separator=" ")
    for term in terms:
        if " " in term:
            mwe_tokenizer.add_mwe(tuple(term.split()))

    number_of_tokens = 0
    number_of_documents = 0
    number_of_lexicon_terms = 0

    for i, document in enumerate(collection.find()):
        if i % 1000 == 0:
            print("Processed {}".format(i))
        tokens = [token for token in mwe_tokenizer.tokenize(word_tokenize(document[text_field].lower()))]
        number_of_documents += 1
        number_of_tokens += len(tokens)
        for token in tokens:
            if token in terms:
                number_of_lexicon_terms += 1
    print(f"Results for db {db_name}, collection {db_collection}")
    print(f"Number of tokens: {number_of_tokens}")
    print(f"Number of documents: {number_of_documents}")
    print(f"Number of lexicon terms: {number_of_lexicon_terms}")
    print(f"Abusive ratio - terms to tokens: {number_of_lexicon_terms/number_of_tokens}")
    print(f"Abusive ratio - terms to documents: {number_of_lexicon_terms/number_of_documents}")


if __name__ == "__main__":
    #db_url = "mongodb://localhost:27017"
    db_url = "mongodb://dsg.foi.se:27017"
    db_name = "stormfront"
    db_collection = "comments"
    #lexicon_dir = "/Users/fredriko/PycharmProjects/alw2/lexicons/lexicons_preprocessed"
    lexicon_dir = "/home/fredrik/source/alw2/lexicons/lexicons_preprocessed"
    process(db_url, db_name, db_collection, lexicon_dir)
