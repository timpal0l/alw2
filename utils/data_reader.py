import time

import numpy as np
import pymongo
from pymongo import MongoClient
from sklearn.utils import shuffle

mongo_object = MongoClient('spindel.foi.se')


def get_training_data_sklearn(database, collection, text_attribute_name='text', label_attribute_name='label',
                              shuffle_data=True, max_num_documents=12000):
    X, y = [], []

    if max_num_documents:
        cursor = mongo_object[database][collection].find(no_cursor_timeout=True).limit(max_num_documents)
    else:
        cursor = mongo_object[database][collection].find(no_cursor_timeout=True)

    for doc in cursor:
        try:
            X.append(doc[text_attribute_name]), y.append(doc[label_attribute_name])
        except KeyError:
            pass
        except pymongo.errors.AutoReconnect as e:
            time.sleep(30)

    if shuffle_data:
        return shuffle(X, y, random_state=0)  # @Todo change me back 66 works
    else:
        return X, y


def get_training_vectors_sklearn(database, collection, vec_attribute_name, label_attribute_name='label',
                                 shuffle_data=True, max_num_documents=12000):
    X, y = [], []

    if max_num_documents:
        cursor = mongo_object[database][collection].find(no_cursor_timeout=True).limit(max_num_documents)
    else:
        cursor = mongo_object[database][collection].find(no_cursor_timeout=True)

    for doc in cursor:
        try:
            X.append(np.asarray(doc[vec_attribute_name])), y.append(doc[label_attribute_name])
        except KeyError:
            pass
        except pymongo.errors.AutoReconnect as e:
            time.sleep(30)

    if shuffle_data:
        return shuffle(X, y, random_state=0)
    else:
        return X, y
