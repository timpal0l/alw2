from scipy import sparse
from sklearn.base import BaseEstimator, TransformerMixin
from utils.data_reader import get_training_vectors_sklearn


class MongoVector(BaseEstimator, TransformerMixin):
    """
    Extracs a predinfed vector from mongodb stored as a list
    and makes em work with the scipy sparse matrix, which is default på the scikit vectorizers. This makes it possible
    to stack em inside the featureunion.
    """

    def __init__(self):
        pass

    def transform(self):
        X, _ = get_training_vectors_sklearn(database='ALW2018', collection='hatespeech_tweets',
                                            vec_attribute_name='{}_vector'.format('reddit_doc2vec'))

        return sparse.csr_matrix(X)

    def fit(self, df, y=None):
        """Returns `self` unless something different happens in train and test"""
        return self
