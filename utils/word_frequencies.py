import os
from collections import Counter

from nltk.tokenize import word_tokenize, MWETokenizer
from pymongo import MongoClient

mongo_object = MongoClient('dsg.foi.se')
print(os.listdir())
os.chdir('lexicons/lexicons_preprocessed')
mwt = MWETokenizer(separator=' ')


def get_relative_term_freqs(database, collection, text_attribute_name='text'):
    lexicons = os.listdir()
    word_counter = Counter()
    token_counter = 0
    lexicons_dic = {}
    lexicons_counts = {'refined_ngram_dict.csv': 0, 'hatebase_dict.csv': 0, 'expandedLexicon.txt': 0,
                       'baseLexicon.txt': 0}

    for lexicon in lexicons:
        with open(lexicon, 'r') as infile:
            terms = set([term.strip() for term in infile.readlines()])
            lexicons_dic[lexicon] = terms

    for lexicon in lexicons_dic:
        terms = lexicons_dic[lexicon]
        for term in terms:
            token = term.split()
            if len(token) > 3:
                mwt.add_mwe((token[0], token[1], token[2], token[3]))
            elif len(token) > 2:
                mwt.add_mwe((token[0], token[1], token[2]))
            elif len(token) > 1:
                mwt.add_mwe((token[0], token[1]))

    for e, doc in enumerate(mongo_object[database][collection].find()):
        
        if (e % 1000 == 0):
            print(e)

        tokens = [token.lower() for token in word_tokenize(doc[text_attribute_name])]
        tokens = mwt.tokenize(tokens)
        nr_of_tokens = len(tokens)
        word_counter.update(tokens)

        for lexicon in lexicons_dic:
            lexicons_counts[lexicon] += sum(word_counter[term] for term in lexicons_dic[lexicon])

        token_counter += nr_of_tokens

    print('database : {}'.format(database))
    print('nr of tokens : {}'.format(token_counter))
    for key, value in lexicons_counts.items():
        lexicons_counts[key] = value / token_counter

    print('relative term freqs')
    print(lexicons_counts)

get_relative_term_freqs('stormfront', 'comments')
