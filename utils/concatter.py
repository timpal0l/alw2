import warnings

from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import ShuffleSplit, cross_val_score
from sklearn.pipeline import make_pipeline, FeatureUnion

from utils.data_reader import get_training_data_sklearn
from utils.mongovector import MongoVector

warnings.filterwarnings('ignore')
estimator = LogisticRegression()

DATABASE = 'alw2018'
collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']

models = ['reddit_doc2vec']

for collection in collections:
    for model in models:
        print('getting data')
        X, y = get_training_data_sklearn(database=DATABASE, collection=collection, max_num_documents=4500)
        char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 4), use_idf=True, norm='l2',
                                          analyzer='char', smooth_idf=True, strip_accents='unicode',
                                          sublinear_tf=True)

        word_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                          analyzer='word', smooth_idf=True, strip_accents='unicode',
                                          sublinear_tf=True)

        # mongo_vector = MongoVector()

        features = FeatureUnion([('chars', char_vectorizer), ('words', word_vectorizer), ])

        scoring = 'f1_weighted'
        test_size = 0.2
        cv = ShuffleSplit(n_splits=10, test_size=test_size, random_state=0)
        pipeline = make_pipeline(features, estimator)
        pipeline.fit(X=X, y=y)
