# The state-of-the-art for the data sets used

This is a work-in-progress file to compile information about the soa for the data sets we're using.

## 1. Data set - Wikipedia detox

Cite as follows:

```
@article{Wulczyn2017,
author = "Ellery Wulczyn and Nithum Thain and Lucas Dixon",
title = "{Wikipedia Talk Labels: Personal Attacks}",
year = "2017",
month = "2",
url = "https://figshare.com/articles/Wikipedia_Talk_Labels_Personal_Attacks/4054689",
doi = "10.6084/m9.figshare.4054689.v6"
}
```

[Link to paper: "Ex Machina: Personal Attacks Seen at Scale"](http://papers.www2017.com.au.s3.amazonaws.com/proceedings/p1391.pdf)

From the paper, in the section called "Choosing a threshold": "For the following analyses, we then use the equal-error
threshold over the union of the development and tests sets. At this
threshold (t = 0.425), the precision is 0.63, the recall (e.g truepositive
rate) is 0.63 and the false-positive rate is 0.0034."

Thus, the authors reach an F1-score which is also 0.63.

### 1.1. Relevant paper - Agrawal and Awekar, 2018. "Deep Learning for Detecting Cyberbullying Across Multiple Social Media Platforms"

Agrawal S., Awekar A. (2018) Deep Learning for Detecting Cyberbullying Across Multiple 
Social Media Platforms. In: Pasi G., Piwowarski B., Azzopardi L., Hanbury A. (eds) 
Advances in Information Retrieval. ECIR 2018. Lecture Notes in Computer Science, vol 10772. Springer, Cham

[Link to the paper](https://pdfs.semanticscholar.org/32b3/5252693f859e55a8a7945cf8947bfca7c867.pdf)

[Link to GitHub repository](https://github.com/sweta20/Detecting-Cyberbullying-Across-SMPs)

This paper reports the best results on Wikipedia "Personal attack" that I've seen so far. 
The twitter data set they use in the paper is the same we use.

"Previous best F1 scores for Wikipedia [18] and Twitter
[2] datasets were 0.68 and 0.93 respectively. We achieve F1 scores of 0.94
for both these datasets using BLSTM with attention and feature level transfer
learning (Table 9)."

### 1.2. Relevant paper - Chu and Joe. "Comment Abuse Classification with Deep Learning"
[Link to paper](https://pdfs.semanticscholar.org/30b8/2ff201bcacc8b3e1dbecfd79308f3137706f.pdf)

"Our models improve upon
previous results from non-deep learning machine-learning models, and we find
that a CNN with character-level embeddings reaches the highest performance."

Baseline results: Linear regression F1 at 0.45, deep neural network F1 at 0.54.

Best result: CNN with character embeddings F1 at 0.73.

### 1.3. Perhaps a relevant paper - Kuehler et al. "Paying attention to toxic comments online."
[Link to paper](https://web.stanford.edu/class/cs224n/reports/6856482.pdf)

Kuehler et al do not use the same evaluation metric as we do (AUC/ROC instead of F1), thus the results are not comparable. However, they carry
out a thourough analysis of the detection of toxic online comments, using the wikipedia detox data set as one of two sets.
Here's a part of their discussion:

"Our first finding was that, suprisingly, non-neural baseline models based on TFIDF sentence vectors perform quite well."

The AUC/ROC results ranges from 0.94595 (baseline model, unclear whether it's a linear 
regression or naive bayes), to 0.96705 (RNN (GRU) with bi-directional and attention mechanisms).

## 2. Data set - Twitter hatespeech

Cite as follows:

```
@InProceedings{waseem-hovy:2016:N16-2,
  author    = {Waseem, Zeerak  and  Hovy, Dirk},
  title     = {Hateful Symbols or Hateful People? Predictive Features for Hate Speech Detection on Twitter},
  booktitle = {Proceedings of the NAACL Student Research Workshop},
  month     = {June},
  year      = {2016},
  address   = {San Diego, California},
  publisher = {Association for Computational Linguistics},
  pages     = {88--93},
  url       = {http://www.aclweb.org/anthology/N16-2013}
}
```
[Link to the paper: "Hateful Symbols or Hateful People? Predictive Features for Hate Speech Detection on Twitter"](http://www.aclweb.org/anthology/N16-2013)

### 2.1. Relevant paper - Agrawal and Awekar, 2018. "Deep Learning for Detecting Cyberbullying Across Multiple Social Media Platforms"

See above as well. F1 at 0.94 for the twitter data set.

### 2.2. Relevant paper - Badjatiya et al. 2017. "Deep Learning for Hate Speech Detection in Tweets"

Microsoft people carrying out a lot of experiments using deep neural networks on the Waseem and Hovy data set.
Best results reached: LSTM+Random Embedding+GBDT F1 score 0.93.



