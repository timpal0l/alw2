import pickle
import random
from collections import Counter

import numpy as np
from keras.layers import Dense, GRU
from keras.models import Sequential, Model, load_model
from keras.optimizers import Adam
from keras.preprocessing.sequence import pad_sequences
from keras.utils import to_categorical
from numpy import array
from pymongo import MongoClient
from sklearn.preprocessing import normalize


def make_mapping(database, text_type, minfreq=1000):
    client = MongoClient('dsg.foi.se')
    db = client[database]
    database_texts = db[text_type]
    cursor = database_texts.find()
    chars = Counter()
    for text in cursor:
        chars.update(set(list(text['text'].lower())))
    filtered = []
    for key, value in chars.items():
        if value >= minfreq:
            filtered.append(key)
    # +1 since we use 0 for oov characters
    mapping = dict((c, i + 1) for i, c in enumerate(filtered))
    return mapping


def get_data(database, text_type, min_len=20, max_num=1000000):
    data = []
    client = MongoClient('dsg.foi.se')
    db = client[database]
    database_texts = db[text_type]
    cursor = database_texts.find()
    num = 0
    for cur in cursor:
        if num < max_num:
            text = cur['text'].lower().split()
            if len(text) >= min_len:
                data.append(' '.join(text))
                num += 1
    return data


def generate_samples(data_array, mapping, batch_size=64, length=129):
    while True:
        cnt = 0
        sequences = []
        while cnt < batch_size:
            random_id = random.randint(0, len(data_array))
            comment = data_array[random_id]
            for i in range(length, len(comment)):
                seq = comment[i - length:i + 1]
                encoded_seq = []
                for char in seq:
                    if char in mapping:
                        encoded_seq.append(mapping[char])
                    else:
                        encoded_seq.append(0)
                sequences.append(encoded_seq)
                cnt += 1
        vocab_size = len(mapping)
        sequences = pad_sequences(sequences[:batch_size], maxlen=length, truncating='pre')
        X = array([to_categorical(x, num_classes=vocab_size + 1) for x in array(sequences)[:, :-1]])
        y = to_categorical(array(sequences)[:, -1], num_classes=vocab_size + 1)
        # print(X, y)
        yield (X, y)


def gru(vocab_size, max_len=128, dimen=1028):
    model = Sequential()
    model.add(GRU(dimen, input_shape=(max_len, vocab_size), recurrent_dropout=0.1, name='gru_layer'))
    model.add(Dense(vocab_size, activation='softmax'))
    adam = Adam(lr=0.0001, decay=0.01)
    model.compile(loss='categorical_crossentropy', optimizer=adam, metrics=['accuracy'])
    rep_model = Model(inputs=model.input, outputs=model.get_layer('gru_layer').output)
    return model, rep_model


def get_rep(text, mapping, model, seq_length=128):
    encoded_seq = []
    for char in text:
        if char in mapping:
            encoded_seq.append(mapping[char])
        else:
            encoded_seq.append(0)
        encoded = to_categorical(pad_sequences([encoded_seq], maxlen=seq_length, truncating='pre'),
                                 num_classes=len(mapping) + 1)
    model_output = model.predict(encoded)
    return model_output


def make_reps(text_string, mapping, model, seq_length=32):
    chunks = [list(text_string)[i:i + seq_length] for i in range(0, len(list(text_string)), seq_length)]
    rep_array = []
    for chunk in chunks:
        encoded_seq = []
        for char in chunk:
            if char in mapping:
                encoded_seq.append(mapping[char])
            else:
                encoded_seq.append(0)
        encoded = to_categorical(pad_sequences([encoded_seq], maxlen=seq_length, truncating='pre'),
                                 num_classes=len(mapping) + 1)
        rep_array.append(model.predict(encoded))
    summed = np.sum(rep_array, axis=0)
    normalized = normalize(summed)
    return normalized


def save_mapping(mapping, outfile):
    with open(outfile + '.pkl', 'wb') as f:
        pickle.dump(mapping, f, pickle.HIGHEST_PROTOCOL)


def load_mapping(infile):
    with open(infile, 'rb') as f:
        return pickle.load(f)


def dump_vectors_to_mongodb(model_name):
    _mapping = load_mapping('char_map_stormfront.pkl')
    _model = load_model('{}.h5'.format(model_name))
    mongo_object = MongoClient('dsg.foi.se')
    db = mongo_object['alw2018']
    collections = ['hatespeech_tweets', 'wikipedia_detox_aggression',
                   'wikipedia_detox_toxicity', 'wikipedia_detox_attack']

    # collections = db.list_collections() buggar ur på asgard :S

    for collection in collections:
        print(collection)
        # collection_name = collection['name']
        for doc in db[collection].find(no_cursor_timeout=True):
            db[collection].update_one(
                {'_id': doc['_id']},
                {'$set': {'{}_vector'.format(model_name): make_reps(text_string=doc['text'],
                                                                    mapping=_mapping,
                                                                    model=_model)[0].tolist()}}, False, True)


def dump_vectors_to_mongo_all_models(model_name_list):
    for model_name in model_name_list:
        dump_vectors_to_mongodb(model_name)
