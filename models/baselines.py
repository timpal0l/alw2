import os

import numpy as np
from sklearn import metrics
from sklearn.externals import joblib
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics import confusion_matrix
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn.pipeline import FeatureUnion, make_pipeline


class Evaluator:

    def __init__(self, X, y, folds, test_size):

        self.X = X
        self.y = y
        self.folds = folds
        self.test_size = test_size
        self.scores = {'accuracy': [], 'precision': [], 'recall': [], 'f1': []}

    def print_metrics(self):
        print()
        print('--------- Final results for Cross Validation ---------')
        print('Average Accuracy', np.sum(self.scores['accuracy']) / self.folds)
        print('Average Precision', np.sum(self.scores['precision']) / self.folds)
        print('Average Recall', np.sum(self.scores['recall']) / self.folds)
        print('Average F1', np.sum(self.scores['f1']) / self.folds)

    def run(self, model, collection_name, make_vectors=True):
        rs = StratifiedShuffleSplit(self.folds, self.test_size, random_state=42)
        self.scores = {'accuracy': [], 'precision': [], 'recall': [], 'f1': []}
        fold = 1

        print('Number of documents for training', (1 - self.test_size) * len(self.y))
        print('Number of documents for testing', self.test_size * len(self.y))
        print()

        for train_index, test_index in rs.split(X=self.X, y=self.y):
            print('Fold', fold, 'of', self.folds)

            train_x = [self.X[x] for x in train_index]
            train_y = [self.y[y] for y in train_index]
            test_x = [self.X[x] for x in test_index]
            test_y = [self.y[y] for y in test_index]

            _model = model()

            if make_vectors:
                char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                                  analyzer='char', smooth_idf=True,
                                                  sublinear_tf=True, max_features=100)

                word_vectorizer = TfidfVectorizer(min_df=0.001, ngram_range=(1, 4), use_idf=True, norm='l2',
                                                  analyzer='word', smooth_idf=True,
                                                  sublinear_tf=True, token_pattern=u'\w{1,}',max_features=100)

                features = FeatureUnion([
                    ('words', word_vectorizer),
                    #('chars', char_vectorizer),
                ])

                pipeline = make_pipeline(features, _model)
                # print(train_y)
                pipeline.fit(train_x, train_y)
                predicted = pipeline.predict(test_x)
                """words = word_vectorizer.vocabulary_
                kex = []
                for key, value in words.items():
                    kex.append(key)
                kex.sort()
                for k in kex:
                    print(k)
                #exit()"""

            else:
                _model.fit(train_x, train_y)
                predicted = _model.predict(test_x)

            accuracy = metrics.accuracy_score(test_y, predicted)
            precision = metrics.precision_score(test_y, predicted, average='weighted')
            recall = metrics.recall_score(test_y, predicted, average='weighted')
            f1 = metrics.f1_score(test_y, predicted, average='weighted')

            self.scores['accuracy'].append(accuracy)
            self.scores['precision'].append(precision)
            self.scores['recall'].append(recall)
            self.scores['f1'].append(f1)

            print('**** Classification report ****')
            print(metrics.classification_report(test_y, predicted))

            print('**** Confusion matrix ****')
            cm = confusion_matrix(test_y, predicted)
            print(cm)

            print()
            fold += 1
            # sample = train_x[np.argmin(np.abs(pipeline.decision_function(train_x)))]
            # print(sample)
        self.print_metrics()
        # joblib.dump(pipeline, 'KF5_modell3.pkl')

        approach = 'baseline' if make_vectors else 'transfer-learning'
        with open(os.path.join('results', collection_name, 'README.md'), 'a') as outfile:
            outfile.write('## Approach: {} \n \n'.format(approach))
            outfile.write('### Classifier: {} \n \n'.format(model.__name__))
            outfile.write('#### Data set: {} \n \n'.format(collection_name))
            outfile.write('#### {} folds of cross validation with {}% as hold out sets \n \n'.format(self.folds,
                                                                                                     self.test_size * 100))
            outfile.write(
                'Average Accuracy : `{}` \n \n'.format(round(np.sum(self.scores['accuracy']) / self.folds, 4) * 100))
            outfile.write(
                'Average Precision : `{}` \n \n'.format(round(np.sum(self.scores['precision']) / self.folds, 4) * 100))
            outfile.write(
                'Average Recall : `{}` \n \n'.format(round(np.sum(self.scores['recall']) / self.folds, 4) * 100))
            outfile.write('Average F1 : `{}` \n \n'.format(round(np.sum(self.scores['f1']) / self.folds, 4) * 100))
            outfile.write('Number of training samples : `{}` \n \n'.format(len(self.y)))
