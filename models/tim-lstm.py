from collections import Counter

import langdetect
from keras.layers import Dense, LSTM
from keras.models import Sequential
from keras.utils import to_categorical
from numpy import array
from pymongo import MongoClient

mongo_object = MongoClient('dsg.foi.se')
docs = mongo_object['reddit']['comments']
SAMPLES = 120


def make_vocab(_vocab_size=100):
    counter = Counter()
    for doc in docs.find()[0:SAMPLES]:
        try:
            lan = langdetect.detect(doc['text'])
            if lan == 'en':
                counter.update(doc['text'])
        except Exception:
            pass

    def is_ascii(s):
        return all(ord(c) < 128 for c in s)

    with open('vocab.txt', 'w') as vocab_file:
        for char, count in counter.most_common(_vocab_size):
            if is_ascii(char):
                vocab_file.write('{}\n'.format(char))


def generate_sequences(length=10):
    _sequences = list()
    y_size = 1
    for doc in docs.find()[0:SAMPLES]:
        try:
            raw_text = doc['text']
            lan = langdetect.detect(raw_text)
            if lan == 'en':
                for i in range(length, len(raw_text)):
                    seq = raw_text[i - length:i + y_size]
                    _sequences.append(seq)

        except Exception:
            pass

    return _sequences


def char2int(_sequences):
    with open('vocab.txt', 'r') as vocab_file:
        chars = sorted([line.strip() for line in vocab_file.readlines()])
    try:
        chars.remove(''), chars.append(' ')
    except ValueError:
        pass
    print(chars)

    mapping = dict((c, i) for i, c in enumerate(chars))
    _mapped_sequences = list()
    # dump(mapping, open('mapping.pkl', 'wb'))

    for sequence in _sequences:
        encoded_seq = []
        for char in sequence:
            if char in mapping:
                encoded_seq.append(mapping[char])
            else:
                encoded_seq.append(len(mapping) + 1)

        # encoded_seq = [mapping[char] if char in mapping else len(mapping) + 1 for char in sequence]
        _mapped_sequences.append(encoded_seq)

    _vocab_size = len(mapping) + 1
    _mapped_sequences = array(_mapped_sequences)

    return _mapped_sequences, _vocab_size


if __name__ == "__main__":
    make_vocab()
    sequences = generate_sequences()
    mapped_sequences, vocab_size = char2int(sequences)
    print(sequences)
    X, y = mapped_sequences[:, :-1], mapped_sequences[:, -1]

    mapped_sequences = []
    for x in X:
        print(x)
        mapped_sequences.append(to_categorical(x))

    #mapped_sequences = [to_categorical(x, num_classes=vocab_size) for x in X]
    X = array(mapped_sequences)
    y = to_categorical(y, num_classes=vocab_size)

    # define model
    model = Sequential()
    model.add(LSTM(75, input_shape=(X.shape[1], X.shape[2])))
    model.add(Dense(vocab_size, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(X, y, epochs=100, verbose=2)
