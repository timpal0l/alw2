import os
import warnings

import matplotlib
import numpy as np
from sklearn.dummy import DummyClassifier
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import ShuffleSplit
from sklearn.model_selection import learning_curve, cross_val_score
from sklearn.pipeline import make_pipeline, FeatureUnion
from random import shuffle
from utils.data_reader import get_training_data_sklearn, get_training_vectors_sklearn

matplotlib.use('agg')
import matplotlib.pyplot as plt

warnings.filterwarnings('ignore')

DATABASE = 'alw2018'
DATABASE = 'genders_sv'

collections = ['hatespeech_tweets', 'wikipedia_detox_aggression', 'wikipedia_detox_attack', 'wikipedia_detox_toxicity']
collections = ['comments',]

models = ['reddit_ngrams', 'stormfront_ngrams', 'Ngrams-D2V-R', 'SentiNeuron', 'stormfront_reps_32', 'reddit_reps_32',
          'stormfront_fasttext', 'stormfront_doc2vec',
          'reddit_fasttext', 'reddit_doc2vec', 'Ngrams-D2V-R-SentiNeuron']

model_name_labels = ['Ngrams', 'Ngrams-R', 'Ngrams-S', 'Ngrams-D2V-R', 'SentiNeuron', 'LM-S-1024', 'LM-R-1024', 'FT-S',
                     'D2V-S', 'FT-R', 'D2V-R', 'Ngrams-D2V-R-SentiNeuron']

color_map = {}
colors = (
    "#F8766D" "#E58700" "#C99800" "#A3A500" "#6BB100" "#00BA38" "#00BF7D" "#00C0AF" "#00BCD8" "#00B0F6" "#619CFF" "#B983FF" "#E76BF3" "#FD61D1" "#FF67A4" "#F1C40F").split(
    '#')

collections = ['comments', ]
models = ['elmo']
model_name_labels = ['Ngrams', 'ELMO']


# add one more color for top concat v-line

colors.remove('')
shuffle(colors)

for e, color in enumerate(colors):
    color_map[e] = "#" + color

# plt.style.use('classic')

# os.chdir('..')

for c, collection in enumerate(collections):
    plt.axes(xscale='log')
    print('Collection {}'.format(collection))
    estimator = LogisticRegression()

    X_ngrams, y_ngrams = get_training_data_sklearn(database=DATABASE, collection=collection)
    char_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 4), use_idf=True, norm='l2',
                                      analyzer='char', smooth_idf=True, strip_accents='unicode',
                                      sublinear_tf=True)

    word_vectorizer = TfidfVectorizer(min_df=0.001, max_df=0.8, ngram_range=(1, 2), use_idf=True, norm='l2',
                                      analyzer='word', smooth_idf=True, strip_accents='unicode',
                                      sublinear_tf=True)

    features = FeatureUnion([('chars', char_vectorizer), ('words', word_vectorizer)])
    pipeline_dummy = make_pipeline(features, DummyClassifier())

    scoring = 'f1_weighted'
    test_size = 0.2
    cv = ShuffleSplit(n_splits=5, test_size=test_size, random_state=0)
    train_size = [n for n in np.logspace(2, 5, 50, dtype='int') if n < (len(y_ngrams) - (len(y_ngrams) * test_size))]
    print('Train steps {}'.format(train_size))

    pipeline_ngrams = make_pipeline(features, estimator)
    train_sizes_ngrams, train_scores_ngrams, test_scores_ngrams = learning_curve(pipeline_ngrams, X_ngrams,
                                                                                 y_ngrams, cv=cv,
                                                                                 scoring=scoring, n_jobs=-1,
                                                                                 train_sizes=train_size, verbose=1)

    test_mean_ngrams = np.mean(test_scores_ngrams, axis=1)
    test_std_ngrams = np.std(test_scores_ngrams, axis=1)

    # ngram_label = (train_size[-2:][0], np.mean(test_mean_ngrams[-1:][0]))
    plt.plot(train_sizes_ngrams, test_mean_ngrams, color=color_map[0],
             label='{} ({})'.format(model_name_labels[0], round(np.mean(test_mean_ngrams[-1:][0]), 3)),
             linewidth=1)
    plt.fill_between(train_sizes_ngrams, test_mean_ngrams - test_std_ngrams, test_mean_ngrams + test_std_ngrams,
                     color=color_map[0], alpha=0.1)

    # plt.annotate(str(round(np.mean(test_mean_ngrams[-1:][0]), 3)), xy=ngram_label, xytext=ngram_label)

    dummy_baseline = np.mean(cross_val_score(pipeline_dummy, X_ngrams, y_ngrams, cv=cv, scoring=scoring, n_jobs=-1))
    # dummy_label = (train_size[-2:][0], dummy_baseline)
    # plt.annotate(str(round(dummy_baseline, 3)), xy=dummy_label, xytext=dummy_label)
    # plt.axhline(y=dummy_baseline, label='Baseline ({})'.format(round(dummy_baseline, 3)), linestyle='--', linewidth=1,
    #             color='black')

    # print('Dummy_baseline {}'.format(dummy_baseline))


    if collection == 'hatespeech_tweets' or collection == 'comments':
        lexicon1 = round(0.6643195301162463, 3)
        lexicon2 = round(0.7292212878459345, 3)
        lexicon3 = round(0.6471279142034335, 3)
        lexicon4 = round(0.6264789442935998, 3)
        # add concat

    elif collection == 'wikipedia_detox_attack':
        lexicon1 = round(0.8063704379956217, 3)
        lexicon2 = round(0.7578945465876298, 3)
        lexicon3 = round(0.797822968965962, 3)
        lexicon4 = round(0.7713396439397628, 3)
        # add concat

    elif collection == 'wikipedia_detox_aggression':
        lexicon1 = round(0.7952568434680325, 3)
        lexicon2 = round(0.7506330060239987, 3)
        lexicon3 = round(0.7772903201368334, 3)
        lexicon4 = round(0.7492374263529316, 3)
        # add concat

    elif collection == 'wikipedia_detox_toxicity':
        lexicon1 = round(0.8310603646057113, 3)
        lexicon2 = round(0.7825681898561165, 3)
        lexicon3 = round(0.8241302878703001, 3)
        lexicon4 = round(0.803972629571045, 3)
        # add concat

    plt.axhline(y=lexicon1, label='Lexicon #1 ({})'.format(lexicon1), linestyle='--', linewidth=1,
                color=color_map[1])
    plt.axhline(y=lexicon2, label='Lexicon #2 ({})'.format(lexicon2), linestyle='--', linewidth=1,
                color=color_map[2])
    plt.axhline(y=lexicon3, label='Lexicon #3 ({})'.format(lexicon3), linestyle='--', linewidth=1,
                color=color_map[3])
    plt.axhline(y=lexicon4, label='Lexicon #4 ({})'.format(lexicon4), linestyle='--', linewidth=1,
                color=color_map[4])
    # add concat

    for t, transfer_model in enumerate(models):
        print('Language model {}'.format(transfer_model))
        X_transfer, y_transfer = get_training_vectors_sklearn(database=DATABASE, collection=collection,
                                                              vec_attribute_name='{}_vector'.format(transfer_model))

        train_sizes_transfer, train_scores_transfer, test_scores_transfer = learning_curve(estimator,
                                                                                           X_transfer,
                                                                                           y_transfer, cv=cv,
                                                                                           scoring=scoring, n_jobs=-1,
                                                                                           train_sizes=train_size,
                                                                                           verbose=1)
        print('Language model {} done'.format(transfer_model))

        test_mean_transfer = np.mean(test_scores_transfer, axis=1)
        test_std_transfer = np.std(test_scores_transfer, axis=1)

        # transfer_label = (train_size[-2:][0], np.mean(test_mean_transfer[-1:][0]))
        plt.plot(train_sizes_transfer, test_mean_transfer, color=color_map[t + 5],
                 label='{} ({})'.format(model_name_labels[t + 1], round(np.mean(test_mean_transfer[-1:][0]), 3)),
                 linewidth=1)

        # plt.annotate(str(), xy=transfer_label, xytext=transfer_label)

        plt.fill_between(train_sizes_transfer, test_mean_transfer - test_std_transfer,
                         test_mean_transfer + test_std_transfer, color=color_map[t + 5], alpha=0.1)

    plt.title("Dataset: {}".format(collection))
    plt.xlabel("Labeled Training Examples"), plt.ylabel(scoring)  # , plt.legend(loc="best")
    plt.rcParams["figure.figsize"] = [6.4, 4.8]
    plt.legend(fontsize=8, loc="lower center")
    plt.tight_layout()
    path = os.path.join('results', collection, '{}_learning_curves.png'.format(collection))
    plt.savefig(path)
    plt.gcf().clear()
